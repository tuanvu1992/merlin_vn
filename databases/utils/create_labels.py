import os
import sys

cwd = os.getcwd() + '/'

fal_path = cwd + 'fal/'
full_path = cwd + 'full/'
state_path = cwd + 'label_state_align/'
phone_path = cwd + 'label_phone_align/'
lab_path = cwd + 'lab/'
id_list_fp = open(cwd + 'file_id_list_full.scp', 'w')

fal_dir = os.listdir(fal_path)
for fname in fal_dir:
	fal_fp = open(fal_path + fname, 'r')
	full_fp = open(full_path + fname, 'r')
	state_fp = open(state_path + fname, 'w')
	phone_fp = open(phone_path + fname, 'w')
	lab_fp = open(lab_path + fname, 'w')
	print 'Procesing file', fname, '...'
	id_list_fp.write('%s\n' % (fname.replace('.lab', '')))

	phone_start = 0
	lab_fp.write('#\n')
	while True:		
		full_line = full_fp.readline()
		if(full_line == ''): break
		name = full_line.split(' ')[2].strip()
		
		for state in range(2,7):
			line = fal_fp.readline()		
			tokens = line.split(' ')
			state_fp.write('%s %s %s[%i]\n' % (tokens[0], tokens[1], name, state))
		phone_end = line.split(' ')[1]
		phone_fp.write('%s %s %s\n' % (phone_start, phone_end, name))
		lab_fp.write('%.3f 125 %s' % (float(phone_end)/10000000, tokens[2].replace('[6]','')))

		phone_start = phone_end



print 'Finished'