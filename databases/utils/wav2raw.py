'''
Created on Nov 10, 2014

@author: dtuan
'''
import os
lsFile=os.listdir('wavn')
for nfile in lsFile:
    rfile=nfile.replace('wav','raw')
    command = 'sox -r 16000 -c 1 -e signed-integer -b 16 '+'wavn/'+nfile+' rawvn/'+rfile
    os.system(command)