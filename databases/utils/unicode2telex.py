﻿import re, string
import codecs
import sys

encoding = 'utf8'

def Unicode2Telex(word):
	SIGN = ''
	wdout = ''
	for char in word: 
		if char in list(u'àầằìỳùừèềồờò'): SIGN = 'f'
		elif char in list(u'ãẫẵĩỹũữẽễỗỡõ'): SIGN = 'x'
		elif char in list(u'ảẩẳỉỷủửẻểổởỏ'): SIGN = 'r'
		elif char in list(u'áấắíýúứéếốớó'): SIGN = 's'
		elif char in list(u'ạậặịỵụựẹệộợọ'): SIGN = 'j'
	for char in word: 
		if char==u"đ":wdout = wdout + "dd" 
		elif char in list(u'àãảáạ'):wdout = wdout + "a"
		elif char in list(u'âầẫẩấậ'):wdout = wdout + "aa"
		elif char in list(u'ăằẵẳắặ'):wdout = wdout + "aw"
		elif char in list(u'ìĩỉíị'):wdout = wdout + "i"
		elif char in list(u'ỳỹỷýỵ'):wdout = wdout + "y"
		elif char in list(u'ùũủúụ'):wdout = wdout + "u"
		elif char in list(u'ưừữửứự'):wdout = wdout + "uw"
		elif char in list(u'èẽẻéẹ'):wdout = wdout + "e"
		elif char in list(u'êềễểếệ'):wdout = wdout + "ee"
		elif char in list(u'ôồỗổốộ'):wdout = wdout + "oo"
		elif char in list(u'ơờỡởớợ'):wdout = wdout + "ow"
		elif char in list(u'òõỏóọ'):wdout = wdout + "o"
		elif ord(char) > 127: wdout = wdout
		else: wdout = wdout + char.encode("ascii")
	wdout = wdout + SIGN
		ostr = ''
		for word in text.split():
			ostr += Unicode2Telex(word) + ' '
	return wdout

def main(argv=None):
	if argv is None:	argv = sys.argv
	in_fp = codecs.open('prompts','r','utf8')
	out_fp = codecs.open('prompts_telex', 'w', 'utf8')
	for text in iter(in_fp.readline, u''):		
		out_fp.write(ostr.strip() + '\n')

if __name__ == "__main__":    sys.exit(main())