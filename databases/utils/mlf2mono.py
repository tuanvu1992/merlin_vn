import codecs
import os
from sent2gen import *
mlf = codecs.open('rec.mlf','r')
mlf.readline()
fname = str()
txt = str()
mono = str()
full = str()
prev_is_sp = False
prev_sp_start = str()
prev_sp_stop = str()
for line in iter(mlf.readline,''):
	token = line.split()
	if(len(token) == 1):
		# End of sentence
		if(token[0] == '.'):
			# Create txt file
			txt_fp = codecs.open('txt/'+fname+'.txt', 'w')
			txt_fp.write(txt.strip())
			# Create gen file
			mlf = sent2mlf(txt.strip(), 'gen/' + fname + '.lab')
			mlf = addInfo(mlf)
			infoMLF2gen(mlf)
			# Create mono label
			mono_fp = codecs.open('mono/'+ fname + '.lab','w')
			mono_fp.write(mono.strip())
			continue
		# Start of sentence
		else:
			fname = (token[0])[3:-5]
			txt = ''
			mono = ''
			print 'Creating ' + fname +'.lab'
			continue
	else:
		if(len(token) == 5):
			if(token[4] not in {'sil','sp','pau'}):
				if(prev_is_sp):
					txt += 'sp '
					mono += prev_sp_start + ' ' + prev_sp_stop + ' ' + 'sp\n'
					prev_is_sp = False
				txt += token[4] + ' '
			elif(token[0] != token[1]):
				if(prev_is_sp):
					token[0] = prev_sp_start
					prev_is_sp = False
				txt += token[4] + ' '
		if(token[0] != token[1]):
			c_phn = str();
			if(token[2] == 'sp'): 
				prev_is_sp = True
				prev_sp_start = token[0]
				prev_sp_stop = token[1]
			else:
				phns = token[2].replace('+','-').split('-')
				if(len(phns) != 1): c_phn = phns[1]
				else: c_phn = phns[0] 
				mono += token[0] + ' ' + token[1] + ' ' + c_phn + '\n'

# Create full label
print 'Create full label...'
for fname in os.listdir('gen/'):
	print fname
	full = str()
	mono_fp = codecs.open('mono/' + fname, 'r')
	gen_fp = codecs.open('gen/' + fname, 'r')
	for gen_line in iter(gen_fp.readline, ''):
		mono_token = mono_fp.readline().split()
		full += mono_token[0] + ' ' + mono_token[1] + ' ' + gen_line
	codecs.open('full/' + fname, 'w').write(full)
print 'Finished!'

					

