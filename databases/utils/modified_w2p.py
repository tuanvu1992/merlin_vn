import os
import sys

base_vowel_list = ['a', 'e', 'i', 'o', 'u']

def get_vowel(w):
	for i in range(1, len(w)):
		for j in range(len(base_vowel_list)):
			if base_vowel_list[j] in w[i]:
				return i
	print "Error"


fid = open("w2p_old", 'r')
fod = open("w2p_with_tone", 'w')

lines = fid.readlines()
fid.close()

tone_list = ['s', 'f', 'r', 'x', 'j']

for line in lines[:5840]:
	s = line.strip().split(' ')
	tone = s[0][-1]
	vowel_index = int()
	phone_with_tone = str()
	if tone in tone_list:
		vowel_index = int(get_vowel(s))
		phone_with_tone = s[vowel_index] + str(tone_list.index(tone) + 1)
		
	else:
		vowel_index = int(get_vowel(s))
		phone_with_tone = s[vowel_index]
	s[vowel_index] = phone_with_tone
	fod.write(" ".join(str(x) for x in s) + "\n")
fod.close()