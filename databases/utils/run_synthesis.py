﻿import codecs, string, glob, os, tempfile, sys
from VnTokenize import *
MLF_FORCE = 'force.mlf.utf8'
silence = ['silence','sil','pau','sp']
phnend = ['iz','uz','mz','nz','ngz','tc','kc','pc']
w2p = map(lambda x: x.rstrip(), codecs.open('w2p','r','utf8').readlines())
wlist = map(lambda x: x.split()[0],w2p)

def standardization(str):
	if str[-1] in string.punctuation: str = str[:-1]
	return str.lower().\
		replace(u'à',u'à').replace(u'ã',u'ã').replace(u'ả',u'ả').replace(u'á',u'á').replace(u'ạ',u'ạ').\
		replace(u'ằ',u'ằ').replace(u'ẵ',u'ẵ').replace(u'ẳ',u'ẳ').replace(u'ắ',u'ắ').replace(u'ặ',u'ặ').\
		replace(u'ầ',u'ầ').replace(u'ẫ',u'ẫ').replace(u'ẩ',u'ẩ').replace(u'ấ',u'ấ').replace(u'ậ',u'ậ').\
		replace(u'ỳ',u'ỳ').replace(u'ỹ',u'ỹ').replace(u'ỷ',u'ỷ').replace(u'ý',u'ý').replace(u'ỵ',u'ỵ').\
		replace(u'ì',u'ì').replace(u'ĩ',u'ĩ').replace(u'ỉ',u'ỉ').replace(u'í',u'í').replace(u'ị',u'ị').\
		replace(u'ù',u'ù').replace(u'ũ',u'ũ').replace(u'ủ',u'ủ').replace(u'ú',u'ú').replace(u'ụ',u'ụ').\
		replace(u'ừ',u'ừ').replace(u'ữ',u'ữ').replace(u'ử',u'ử').replace(u'ứ',u'ứ').replace(u'ự',u'ự').\
		replace(u'è',u'è').replace(u'ẽ',u'ẽ').replace(u'ẻ',u'ẻ').replace(u'é',u'é').replace(u'ẹ',u'ẹ').\
		replace(u'ề',u'ề').replace(u'ễ',u'ễ').replace(u'ể',u'ể').replace(u'ế',u'ế').replace(u'ệ',u'ệ').\
		replace(u'ò',u'ò').replace(u'õ',u'õ').replace(u'ỏ',u'ỏ').replace(u'ó',u'ó').replace(u'ọ',u'ọ').\
		replace(u'ờ',u'ờ').replace(u'ỡ',u'ỡ').replace(u'ở',u'ở').replace(u'ớ',u'ớ').replace(u'ợ',u'ợ').\
		replace(u'ồ',u'ồ').replace(u'ỗ',u'ỗ').replace(u'ổ',u'ổ').replace(u'ố',u'ố').replace(u'ộ',u'ộ')
		
def Unicode2Telex(word):
	SIGN = ''
	wdout = ''
	for char in word: 
		if char in list(u'àầằìỳùừèềồờò'): SIGN = 'f'
		elif char in list(u'ãẫẵĩỹũữẽễỗỡõ'): SIGN = 'x'
		elif char in list(u'ảẩẳỉỷủửẻểổởỏ'): SIGN = 'r'
		elif char in list(u'áấắíýúứéếốớó'): SIGN = 's'
		elif char in list(u'ạậặịỵụựẹệộợọ'): SIGN = 'j'
	for char in word: 
		if char==u"đ":wdout = wdout + "dd" 
		elif char in list(u'àãảáạ'):wdout = wdout + "a"
		elif char in list(u'âầẫẩấậ'):wdout = wdout + "aa"
		elif char in list(u'ăằẵẳắặ'):wdout = wdout + "aw"
		elif char in list(u'ìĩỉíị'):wdout = wdout + "i"
		elif char in list(u'ỳỹỷýỵ'):wdout = wdout + "y"
		elif char in list(u'ùũủúụ'):wdout = wdout + "u"
		elif char in list(u'ưừữửứự'):wdout = wdout + "uw"
		elif char in list(u'èẽẻéẹ'):wdout = wdout + "e"
		elif char in list(u'êềễểếệ'):wdout = wdout + "ee"
		elif char in list(u'ôồỗổốộ'):wdout = wdout + "oo"
		elif char in list(u'ơờỡởớợ'):wdout = wdout + "ow"
		elif char in list(u'òõỏóọ'):wdout = wdout + "o"
		elif ord(char) > 127: wdout = wdout
		else: wdout = wdout + char.encode("ascii")
	wdout = wdout + SIGN
	return wdout
	
def sent2mlf(SentIn, Fname):
	ss = VnTokenize()
	textchunk = ss.commonProcess(SentIn)
	textchunk = ss.sentSplit(textchunk)
	textchunk = ss.syllSplit(textchunk)
	words = textchunk.split()
	utt = [Fname]
	utt.append(['silence', ['0','1','sil']])
	for word in words:
		word = Unicode2Telex(standardization(word))
		if word not in wlist:
			utt.append(['silence', ['0','1','sil']])
			continue
		phns = w2p[wlist.index(word)].split()[1:]
		w = [word]
		for phn in phns: w.append(['0','1',phn])
		utt.append(w)
	utt.append(['silence', ['0','1','sil']])
	mlf = [utt]	
	return mlf

def infoMLF2gen(mlf):
	for utt in mlf:
		outf = open(utt[0],'w')
		for word in utt[1:]:
			#outf.write(word[0]+'\n')
			for phn in word[1:]:
				adding = 'x'
				e5 = 'x'
				e6 = 'x'
				if phn[2] not in silence: 
					adding = '1'
					e5 = str(int(phn[10])-1)
					e6 = str(int(phn[18])-1)
				for i in range(2,7):					
					mystr = "0 0 %s^%s-%s+%s=%s@%s_%s\
/A:%s_%s_%s\
/B:%s-%s-%s@%s-%s&%s-%s#%s-%s$%s-%s!%s-%s;%s-%s|%s\
/C:%s+%s+%s\
/D:%s_%s\
/E:%s+%s@%s+%s&%s+%s#%s+%s\
/F:%s_%s\
/G:%s_%s\
/H:%s=%s@%s=%s|%s\
/I:%s=%s\
/J:%s+%s-%s[%s]\n" % \
(phn[3],phn[4],phn[2],phn[14],phn[15],phn[7],phn[8],\
phn[13],'0',phn[5],\
phn[12],'0',phn[9],'1','1',phn[10],phn[18],phn[19],phn[20],phn[21],phn[22],phn[23],phn[24],phn[25],phn[26],phn[27],\
phn[29],'0',phn[16],\
phn[6],'1',\
phn[11],adding,phn[10],phn[18],e5,e6,adding,adding,\
phn[17],'1',\
'0','0',\
phn[28],phn[28],'1', '1', 'L-L%',\
'0','0',\
phn[28],phn[28],'1', i)
					outf.write(mystr)	

def getTone(wordin):
	word = unicode(wordin,'utf8')
	tfalling = set(u'àầằìùừèềòồờ')
	tbroken = set(u'ãẫẵĩũữẽễõỗỡ')
	tcurve = set(u'ảẩẳỉủửẻểỏổở')
	trising = set(u'áấắíúứéếóốớ')
	tdrop = set(u'ạậặịụựẹệọộợ')
	wset = set(word)
	tone = 0 #level
	if len(wset.intersection(tfalling))>0: tone = 1
	elif len(wset.intersection(tbroken))>0: tone = 2
	elif len(wset.intersection(tcurve))>0: tone = 3
	elif len(wset.intersection(trising))>0: tone = 4
	elif len(wset.intersection(tdrop))>0: tone = 5
	# in case of TELEX:
	if word.endswith('f'): tone = 1
	elif word.endswith('x'): tone = 2
	elif word.endswith('r'): tone = 3
	elif word.endswith('s'): tone = 4
	elif word.endswith('j'): tone = 5
	return tone

def addInfo(mlf):
	for utt in mlf:
		word = ['silence']
		L_word = ['x']
		LL_phn = 'x'
		L_phn = 'x'
		phn = ['0','0','x']
		SylPosInPharse = 1
		for wordID in range(1,len(utt)):
			if word[0] not in silence: 
				L_word = word
				SylPosInPharse +=1
			word = utt[wordID]
			for phnID in range(1,len(word)):
				LL_phn = L_phn
				L_phn = phn[2]
				phn = word[phnID]
				phn.append(LL_phn)	#LL phn
				phn.append(L_phn)		#L phn
				phn.append(str(len(L_word)-1))	# number of phn (L)
				if len(L_word) == 1: phn.append('0') 	#gpos (L)
				else:	phn.append('content')						#gpos (L)
				if phn[2] in silence: 
					phn.append('x')	# phn postion
					phn.append('x')	# phn postion(reverse)
					phn.append('x')	# number of phn
					phn.append('x') # Syl position in Pharse
					phn.append('x') # gpos
					phn.append('0') # tone
				else: 
					phn.append(str(phnID))						# phn postion 
					phn.append(str(len(word)-phnID))	# phn postion (reverse)
					phn.append(str(len(word)-1))			# number of phn
					phn.append(str(SylPosInPharse)) 	# Syl position in Pharse
					phn.append('content') # gpos
					phn.append(str(getTone(word[0]))) # tone
				phn.append(str(getTone(L_word[0])))  #tone (L)
		word = ['silence']		
		R_word = ['x']
		phn = ['0','0','x']
		R_phn = 'x'
		RR_phn = 'x'
		SylPosInPharseR = 1 #reverse
		for wordID in range(len(utt)-1,0,-1):
			if word[0] not in silence:
				R_word = word
				SylPosInPharseR +=1	
			word = utt[wordID]
			for phnID in range(len(word)-1,0,-1):
				RR_phn = R_phn
				R_phn = phn[2]
				phn = word[phnID]
				phn.append(R_phn)								# R phn
				phn.append(RR_phn)							# RR phn
				phn.append(str(len(R_word)-1))	# number of phn (R)
				if len(R_word) == 1: phn.append('0') #gpos (R)
				else:	phn.append('content')					 #gpos (R)
				if phn[2] in silence: 
					phn.append('x') # Syl position in Pharse (reverse)
					for i in range(9): phn.append('x') # b8-b16
				else: 
					phn.append(str(SylPosInPharseR))	# Syl position in Pharse (reverse)
					for i in range(9): phn.append('0') # b8-b16
				phn.append(str(SylPosInPharse-1))   # number of syll in pharse
				phn.append(str(getTone(R_word[0])))  # tone (R)
	return mlf

def main(argv=None):
	if argv is None:	argv = sys.argv
	text = codecs.open(argv[1],'r','utf8').read()
	mlf = sent2mlf(text, argv[1].replace('.txt','') + '.lab')
	mlf = addInfo(mlf)
	infoMLF2gen(mlf)
if __name__ == "__main__":    sys.exit(main())
