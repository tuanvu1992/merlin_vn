import codecs
import os

for fname in os.listdir('gen/'):
	print fname
	full = str()
	mono_fp = codecs.open('mono/' + fname, 'r')
	gen_fp = codecs.open('gen/' + fname, 'r')
	for gen_line in iter(gen_fp.readline, ''):
		mono_token = mono_fp.readline().split()
		full += mono_token[0] + ' ' + mono_token[1] + ' ' + gen_line
	codecs.open('full/' + fname, 'w').write(full)
print 'Finished!'
		
