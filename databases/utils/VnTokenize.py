﻿import re, string
import codecs
encoding = 'utf8'
gESCAPE = [(".", "_PERIOD_"), (":", "_COLON_")]
#Wordlist = set(map(lambda x: x.rstrip(), codecs.open('VLdict','r',encoding).readlines()))
#VUdict = set(map(lambda x: x.rstrip(), codecs.open('checked','r',encoding).readlines()))
#vnloc = set(map(lambda x: x.rstrip(), codecs.open('Locations.txt','r',encoding).readlines()))
#vnper = set(map(lambda x: x.rstrip(), codecs.open('VnNames.txt','r',encoding).readlines()))
#pronouns = set(map(lambda x: x.rstrip(), codecs.open('Pronoun.txt','r',encoding).readlines()))
#gWordlist = Wordlist.union(VUdict)
gVieUpper = u"ĐÀÃẢÁẠÂẦẪẨẤẬĂẰẴẲẮẶÌĨỈÍỊỲỸỶÝỴÙŨỦÚỤƯỪỮỬỨỰÈẼẺÉẸÊỀỄỂẾỆÒÕỎÓỌÔỒỖỔỐỘƠỜỠỞỚỢ" + string.uppercase
gVieLower = u"đàãảáạâầẫẩấậăằẵẳắặìĩỉíịỳỹỷýỵùũủúụưừữửứựèẽẻéẹêềễểếệòõỏóọôồỗổốộơờỡởớợ" + string.lowercase
#debugF = codecs.open('debug','w',encoding)
gCommonProcess_regexps = [ 
	(re.compile(u'[\r\n]{2,}|\r'),u'\n'), 
	(re.compile(u'[ ]{2,}'),' '), 
	(re.compile(u'[”“]|\'\'|\`\`'),'"'), 
	(re.compile(u'…|(\.\s*){2,}\.'),'...'), 
]
gSentSplit_regexps = [
	(re.compile("""([!"#').?]+) (["'(-]*\s?["""+gVieUpper+string.digits+"""])"""), r"\1\n\2"),
]
gSyllSplit_regexps = [ 
	# Separate punctuation (except period) from words: 
	(re.compile(r'(?=[\(\"\`{\[:;&\#\*@])(.)'), r'\1 '),
	(re.compile(r'(.)(?=[?!)\";}\]\*:@\'])'), r'\1 '), #'''
	(re.compile(r'(?=[\)}\]])(.)'), r'\1 '), 
	(re.compile(r'(.)(?=[({\[])'), r'\1 '), 
	(re.compile(r'((^|\s)\-)(?=[^\-])'), r'\1 '), 
	# Treat double-hyphen as one token: 
	(re.compile(r'([^-])(\-\-+)([^-])'), r'\1 \2 \3'), 
	(re.compile(r'(\s|^)(,)(?=(\S))'), r'\1\2 '), 
	# Only separate comma if space follows: 
	(re.compile(r'(.)(,)(\s|$)'), r'\1 \2\3'), 
	# The dots at the end line as a signle token
	(re.compile(r'([^.])(\.[$\n\r])'), r'\1 \2'), 
	(re.compile(r'([^-])(\-[\s])'), r'\1 \2'), 
	## Separate "No.6" 
	(re.compile(r'([^\W\d]\.)(\d+)'), r'\1 \2'), 
	# Separate words from ellipses 
	(re.compile(r'([^\.]|^)(\.{2,})(.?)'), r'\1 \2 \3'), 
	(re.compile(r'(^|\s)(\.{2,})([^\.\s])'), r'\1\2 \3'), 
	(re.compile(r'([^\.\s])(\.{2,})($|\s)'), r'\1 \2\3'),
	# for TTS
	(re.compile(u'ngày (\d+)[/-](\d+)[/-](\d+)'), r' ngayf \1 thangs \2 nawm \3 '), 
	(re.compile('(\d+)[/-](\d+)[/-](\d+)'), r' ngayf \1 thangs \2 nawm \3 '), 
	(re.compile('%'), u' phần trăm '), 
	(re.compile(' 10 '), u' mười '), 
	(re.compile(' 11 '), u' mười một '), 
	(re.compile(' 12 '), u' mười hai '), 
	(re.compile(' 13 '), u' mười ba '), 
	(re.compile(' 14 '), u' mười bốn '), 
	(re.compile(' 15 '), u' mười năm '), 
	(re.compile(' 16 '), u' mười sáu '), 
	(re.compile(' 17 '), u' mười bẩy '), 
	(re.compile(' 18 '), u' mười tám '), 
	(re.compile(' 19 '), u' mười chín '), 
	(re.compile(' 20 '), u' hai mươi '), 
	(re.compile(' 30 '), u' ba mươi '), 
	(re.compile(' 40 '), u' bốn mươi '), 
	(re.compile(' 50 '), u' năm mươi '), 
	(re.compile(' 60 '), u' sáu mươi '), 
	(re.compile(' 70 '), u' bẩy mươi '), 
	(re.compile(' 80 '), u' tám mươi '), 
	(re.compile(' 90 '), u' chín mươi '), 
	(re.compile('0'), u' không '), 
	(re.compile('1'), u' một '), 
	(re.compile('2'), u' hai '), 
	(re.compile('3'), u' ba '), 
	(re.compile('4'), u' bốn '), 
	(re.compile('5'), u' năm '), 
	(re.compile('6'), u' sáu '), 
	(re.compile('7'), u' bẩy '), 
	(re.compile('8'), u' tám '), 
	(re.compile('9'), u' chín '), 
]
gWordCombine_regexps = [ 
	# combine some pronoun such as: Vu_Tat_Thang
	(re.compile("""([^"] )((["""+gVieUpper+"""]["""+gVieLower+"""]+[\s_]){2,})"""), lambda m: (m.groups()[0]+''.join((m.groups()[1:-1])).strip().replace(' ','_')+' ')),
#	(re.compile(u'((\d+\s[:\-./,]\s)+\d+)$'), lambda m: m.groups()[0].replace(' ','_')),
	(re.compile(u'((\d+\s[:\-./,]\s)+\d+)(\s)'), lambda m: m.groups()[0].replace(' ','_')+m.groups()[-1]),
	(re.compile("""(["""+gVieUpper+"""]+\s\.)[ ]"""),lambda m: m.groups()[0].replace(' ','_')+' '),
]
#'''
class VnTokenize(object):
	def __init__(self, abbreviations=[], escape=gESCAPE):
		self.setAbbreviations(abbreviations)
		self.setEscape(escape)		
	def setAbbreviations(self, abbreviations): self._abbreviations = abbreviations
	def getAbbreviations(self): return self._abbreviations
	def setEscape(self, escape):self._escape = escape
	def getEscape(self):	return self._escape
	def commonProcess(self, text):
		for (regexp, repl) in gCommonProcess_regexps: 
			text = regexp.sub(repl, text) 
			return text	
	def syllSplit(self, text):
		for (regexp, repl) in gSyllSplit_regexps: text = regexp.sub(repl, text) 
		#debugF.write('%s\n' % text)
		return text
	def sentSplit(self, text):	# Fail with citations of multi-sent, i.e.: "One sent. Now, it is second."
		## Escape ABBR
		for abbrev in self.getAbbreviations():
			if text.count(abbrev):
				for t_escapemapping in self.getEscape():
					escabbrev = abbrev.replace(t_escapemapping[0],t_escapemapping[1])
					text = text.replace(abbrev, escabbrev)
		## Apply RE
		for (regexp, repl) in gSentSplit_regexps: sentencestring = regexp.sub(repl, text) 
		## Restore ABBR
		for t_escapemapping in self.getEscape():
			if sentencestring.count(t_escapemapping[1]):
				sentencestring = sentencestring.replace(t_escapemapping[1],t_escapemapping[0])
		return sentencestring
		
if __name__ == '__main__':
	filein = 'test.txt'#raw_input("File: ")
	fileout = filein + '.res'
	outf = codecs.open(fileout,'w',encoding)
	abbreviations = ("prof.", "Prof.")
	ss = VnTokenize(abbreviations=abbreviations)
	textchunk = codecs.open(filein,'r',encoding).read().rstrip()
	textchunk = ss.commonProcess(textchunk)
	outf.write('%s\n' % textchunk)
	textchunk = ss.sentSplit(textchunk)
	textchunk = ss.syllSplit(textchunk)
	