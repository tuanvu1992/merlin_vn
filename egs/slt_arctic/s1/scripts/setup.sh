#!/bin/bash

if test "$#" -ne 1; then
    echo "Usage: ./scripts/setup.sh <voice_directory_name>"
    exit 1
fi

current_working_dir=$(pwd)
merlin_dir=$(dirname $(dirname $(dirname $current_working_dir)))
experiments_dir=${current_working_dir}/experiments

voice_name=$1
voice_dir=${experiments_dir}/${voice_name}

acoustic_dir=${voice_dir}/acoustic_model
duration_dir=${voice_dir}/duration_model
synthesis_dir=${voice_dir}/test_synthesis

mkdir -p ${experiments_dir}
mkdir -p ${voice_dir}
mkdir -p ${acoustic_dir}
mkdir -p ${duration_dir}

if [ "$voice_name" == "vn_kp_full" ]
then
    data_dir=vn_kp_full_data
elif [ "$voice_name" == "vn_mb_full" ]
then
    data_dir=vn_mb_full_data
elif [ "$voice_name" == "jp_nit_full" ]
then
    data_dir=jp_nit_full_data
elif [ "$voice_name" == "vn_mb_demo" ]
then
    data_dir=vn_mb_demo_data
else
    echo "The data for voice name ($voice_name) is not available...please use vn_kp_full or vn_mb_full or vn_mb_demo or jp_nit_full !!"
    exit 1
fi

if [[ ! -f ${data_dir}.zip ]]; then
    cp ${merlin_dir}/databases/archive/${data_dir}.zip ${current_working_dir}
    do_unzip=true
fi
if [[ ! -d ${data_dir} ]] || [[ -n "$do_unzip" ]]; then
    echo "unzipping files......"
    rm -fr ${data_dir}
    rm -fr ${duration_dir}/data
    rm -fr ${acoustic_dir}/data
    unzip -q ${data_dir}.zip
    mv ${data_dir}/merlin_baseline_practice/duration_data/ ${duration_dir}/data
    mv ${data_dir}/merlin_baseline_practice/acoustic_data/ ${acoustic_dir}/data
    mv ${data_dir}/merlin_baseline_practice/test_data/ ${synthesis_dir}
fi
echo "data is ready!"

global_config_file=conf/global_settings.cfg

### default settings ###
echo "MerlinDir=${merlin_dir}" >  $global_config_file
echo "WorkDir=${current_working_dir}" >>  $global_config_file
echo "Voice=${voice_name}" >> $global_config_file
echo "Labels=state_align" >> $global_config_file

if [ "$voice_name" == "vn_kp_full" ] || [ "$voice_name" == "vn_mb_full" ] || [ "$voice_name" == "vn_mb_demo" ]
then
   echo "QuestionFile=vn_cqs_questions_486.hed" >> $global_config_file
elif [ "$voice_name" == "jp_nit_full" ]
then
    echo "QuestionFile=jp_cqs_questions_486.hed" >> $global_config_file
else
    echo "No suiltable question file"
    exit 1
fi

echo "Vocoder=WORLD" >> $global_config_file
echo "SamplingFreq=16000" >> $global_config_file

if [ "$voice_name" == "vn_mb_full" ]
then
    echo "FileIDList=file_id_list_full.scp" >> $global_config_file
    echo "Train=450" >> $global_config_file 
    echo "Valid=30" >> $global_config_file 
    echo "Test=30" >> $global_config_file 


elif [ "$voice_name" == "vn_mb_demo" ]
then
    echo "FileIDList=file_id_list_full.scp" >> $global_config_file
    echo "Train=60" >> $global_config_file 
    echo "Valid=20" >> $global_config_file 
    echo "Test=20" >> $global_config_file

elif [ "$voice_name" == "vn_kp_full" ]
then
    echo "FileIDList=file_id_list_full.scp" >> $global_config_file
    echo "Train=1201" >> $global_config_file 
    echo "Valid=75" >> $global_config_file 
    echo "Test=75" >> $global_config_file 

elif [ "$voice_name" == "jp_nit_full" ]
then
    echo "FileIDList=file_id_list_full.scp" >> $global_config_file
    echo "Train=443" >> $global_config_file 
    echo "Valid=30" >> $global_config_file 
    echo "Test=30" >> $global_config_file 

else
    echo "The data for voice name ($voice_name) is not available...please use vn_kp_full or vn_mb_full or jp_nit_full !!"
    exit 1
fi

echo "Merlin default voice settings configured in $global_config_file"
echo "setup done...!"

